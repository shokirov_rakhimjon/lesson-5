
// Task1
const vowel_count = str => [...str].filter(c => 'aeiou'.includes(c.toLowerCase())).length;

// var str = "The quick brown fox"
// console.log(vowel_count(str));


// Task 2
function daysDiff(CurrentDate)
{
	var TYear=CurrentDate.getTime();
    var TDay=new Date("January, 01, 2022").getTime();
    var DayCount=(TDay-TYear)/(1000*60*60*24);
    DayCount=Math.round(DayCount); 
    return(DayCount);
}

// console.log(daysDiff(new Date('October, 16, 2021')));


// Task 3
function findSymbol (arr) {
    return arr.filter(function (item){
        return !(
        (item.charCodeAt()>47 && item.charCodeAt()<58) || 
        (item.charCodeAt()>64 && item.charCodeAt()<91) || 
        (item.charCodeAt()>96 && item.charCodeAt()<123));
    });
}

// console.log(findSymbol(['a', 'b', '$', 'sa', '(']));


// Task 4
function intersect(a, b) {
    var setB = new Set(b);
    return [...new Set(a)].filter(x => setB.has(x));
  }

// console.log(intersect([1,2,3], [2,3,4,5]));


// Task 5
// var myVar = setTimeout(function(){ alert("Hello") }, 3000);


// Task 6
function sumnatural(arr){
    return arr.reduce(function (result, item){
        if (Number.isInteger(item)){
            return result + item
        }
        return result
    }, 0);
}

// function sumnatural(arr){
//     return arr
//     .filter(a => Number.isInteger(a))
//     .reduce(function (res, itm) {return res+itm;});
// }

// console.log(sumnatural([1, 2, 3, 'a', true, 2.5, 4]));


// Task 7
function countChar (str, chr) {
    return str
    .split("")
    .filter(item => item ===chr)
    .length
}

// console.log(countChar('there was message which had sent by my side', 'y'));
