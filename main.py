import subprocess


def read_usernames():
    try:
        with open('usernames.txt', 'r') as file:
            usernames = file.read()
            return usernames
    except Exception as e:
        print(e)
        return ""


def lower_and_join_usernames(usernames):
    usernames_list = usernames.split('\n')
    usernames_response = []

    if usernames == "" or usernames == " ":
        return []

    for username in usernames_list:
        if username != "":
            usernames_response.append('_'.join(username.lower().split(' ')))

    return usernames_response


def create_git_branches(usernames):
    for username in usernames:
        subprocess.run(["git", "switch", "-c", "{}".format(username)])


def main():
    usernames = read_usernames()
    lowercase_usernames = lower_and_join_usernames(usernames)
    create_git_branches(lowercase_usernames)

if __name__ == "__main__":
    main()
